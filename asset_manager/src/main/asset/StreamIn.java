/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.asset;

import main.asset.util.Utils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author David
 */
public class StreamIn extends BufferedInputStream {

    public StreamIn(File file) throws FileNotFoundException {
        super(new FileInputStream(file));
    }

    public StreamIn(String url) throws FileNotFoundException {
        super(new FileInputStream(new File(url)));
    }

    public byte readByte() throws IOException, StreamEndException {
        int i = this.read();
        if (i == -1) {
            throw new StreamEndException();
        } else {
            return Utils.sign(i);
        }
    }

    public int readInt() throws IOException, StreamEndException {
        byte[] b = new byte[4];
        this.read(b);
        for (byte i : b) {
            if (i == -1) {
                throw new StreamEndException();
            }
        }
        return Utils.bytesToInt(b);
    }

    public float readFloat() throws IOException, StreamEndException {
        byte[] b = new byte[4];
        this.read(b);
        for (byte i : b) {
            if (i == -1) {
                throw new StreamEndException();
            }
        }
        return Utils.bytesToFloat(b);
    }

    public String readString(int length) throws IOException, StreamEndException {
        byte[] b = new byte[length];
        this.read(b);
        for (byte i : b) {
            if (i == -1) {
                throw new StreamEndException();
            }
        }
        return Utils.bytesToString(b);
    }
}
