package main.asset;

//@author David
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class ImageTexture {

    private ByteBuffer textureData;
    private int width;
    private int height;

    public static ImageTexture load(String url) {
        Image tempImage = null;
        InputStream in;
        try {
            in = new FileInputStream(url);
            tempImage = ImageIO.read(in);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImageTexture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(ImageTexture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        BufferedImage image = new BufferedImage(tempImage.getWidth(null), tempImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        image.getGraphics().drawImage(tempImage, 0, 0, null);
        int pixels[] = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
        ByteBuffer buffer = ByteBuffer.allocate(image.getWidth() * image.getHeight() * 4).order(ByteOrder.nativeOrder());
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
                buffer.put((byte) (pixel & 0xFF));               // Blue component
                buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
            }
        }
        buffer.flip();
        return new ImageTexture(buffer, image.getWidth(), image.getHeight());
    }

    private ImageTexture(ByteBuffer data, int width, int height) {
        this.textureData = data;
        this.width = width;
        this.height = height;
    }

    public TextureChunk createChunk(String id) {
        return new TextureChunk(id, width, height, textureData);
    }
}
