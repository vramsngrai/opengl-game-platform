package main.asset.util;

public class Utils {

    public static int unsign(byte value) {
        return value & 0xff;
    }

    public static int[] unsign(byte[] value) {
        int[] result = new int[value.length];
        for (int i = 0; i < value.length; i++) {
            result[i] = value[i] & 0xff;
        }
        return result;
    }

    public static byte sign(int value) {
        return (byte) (value);
    }

    public static byte[] sign(int[] value) {
        byte[] result = new byte[value.length];
        for (int i = 0; i < value.length; i++) {
            result[i] = (byte) (value[i]);
        }
        return result;
    }

    public static byte[] intToBytes(int data) {
        return new byte[]{
                    (byte) (data >>> 24),
                    (byte) (data >>> 16),
                    (byte) (data >>> 8),
                    (byte) (data)
                };
    }

    public static byte[] charToBytes(char data) {
        return new byte[]{
                    (byte) (data >>> 8),
                    (byte) (data)
                };
    }

    public static char bytesToChar(byte[] data) {
        return (char) ((0xff & data[0]) << 8 | (0xff & data[1]));
    }

    public static int bytesToInt(byte[] data) {
        return (0xff & data[0]) << 24 | (0xff & data[1]) << 16 | (0xff & data[2]) << 8 | (0xff & data[3]) << 0;
    }

    public static byte[] floatToBytes(float input) {
        int data = Float.floatToRawIntBits(input);
        return new byte[]{
                    (byte) (data >>> 24),
                    (byte) (data >>> 16),
                    (byte) (data >>> 8),
                    (byte) (data)
                };
    }

    public static float bytesToFloat(byte[] data) {
        int result = (0xff & data[0]) << 24 | (0xff & data[1]) << 16 | (0xff & data[2]) << 8 | (0xff & data[3]) << 0;
        return Float.intBitsToFloat(result);
    }

    public static byte[] stringToBytes(String data) {
        char[] chars = data.toCharArray();
        byte[] binary = new byte[chars.length * 2];
        for (int i = 0; i < chars.length; i++) {
            byte[] temp = Utils.charToBytes(chars[i]);
            binary[(i * 2)] = temp[0];
            binary[(i * 2) + 1] = temp[1];
        }
        return binary;
    }

    public static String bytesToString(byte[] data) {
        char[] chars = new char[data.length / 2];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) ((0xff & data[(i * 2)]) << 8 | (0xff & data[(i * 2) + 1]));
        }
        return new String(chars);
    }

    public static float[] byteArrayToFloatArray(byte[] data) {
        if (data.length % 4 != 0) {
            return null;
        }
        float[] result = new float[data.length / 4];
        for (int i = 0; i < result.length; i++) {
            result[i] = Float.intBitsToFloat((0xff & data[(i * 4)]) << 24 | (0xff & data[(i * 4) + 1]) << 16 | (0xff & data[(i * 4) + 2]) << 8 | (0xff & data[(i * 4) + 3]));
        }
        return result;
    }

    public static byte[] floatArrayToByteArray(float[] data) {
        byte[] result = new byte[data.length * 4];
        for (int i = 0; i < data.length; i++) {
            int temp = Float.floatToRawIntBits(data[i]);
            result[(i * 4)] = (byte) (temp >>> 24);
            result[(i * 4) + 1] = (byte) (temp >>> 16);
            result[(i * 4) + 2] = (byte) (temp >>> 8);
            result[(i * 4) + 3] = (byte) (temp);
        }
        return result;
    }

    public static int[] byteArrayToIntArray(byte[] data) {
        if (data.length % 4 != 0) {
            return null;
        }
        int[] result = new int[data.length / 4];
        for (int i = 0; i < result.length; i++) {
            result[i] = (0xff & data[(i * 4)]) << 24 | (0xff & data[(i * 4) + 1]) << 16 | (0xff & data[(i * 4) + 2]) << 8 | (0xff & data[(i * 4) + 3]);
        }
        return result;
    }

    public static byte[] intArrayToByteArray(int[] data) {
        byte[] result = new byte[data.length * 4];
        for (int i = 0; i < data.length; i++) {
            result[(i * 4)] = (byte) (data[i] >>> 24);
            result[(i * 4) + 1] = (byte) (data[i] >>> 16);
            result[(i * 4) + 2] = (byte) (data[i] >>> 8);
            result[(i * 4) + 3] = (byte) (data[i]);
        }
        return result;
    }
}
