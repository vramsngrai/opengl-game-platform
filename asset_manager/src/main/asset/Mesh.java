package main.asset;

/**
 * @author David Gronlund
 */
public class Mesh {

    public float[] vertices;
    public float[] textureCoordinates;
    public float[] normals;
    public int[] indices;

    public Mesh(int[] indices, float[] vertices, float[] textures, float[] normals) {
        this.indices = indices;
        this.vertices = vertices;
        this.textureCoordinates = textures;
        this.normals = normals;
    }
}
