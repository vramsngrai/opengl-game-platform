package main.asset;

//@author David
import main.asset.util.Utils;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShaderChunk extends Chunk {

    private String vertShader;
    private String fragShader;

    public ShaderChunk(String id, String vertShader, String fragShader) {
        super(id, Chunk.SHADER_CHUNK);
        this.vertShader = vertShader;
        this.fragShader = fragShader;
    }

    public static ShaderChunk load(ByteBuffer b, String id) {
        String vertShader = null;
        String fragShader = null;

        while (b.hasRemaining()) {
            byte type = b.get();
            if (type == 0x00) { //Vertex Shader
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                vertShader = Utils.bytesToString(data);
            } else if (type == 0x01) { //Fragment Shader
                int length = b.getInt();
                byte[] data = new byte[length];
                b.get(data);
                fragShader = Utils.bytesToString(data);
            } else if (type == 0x02) { //Geometry Shader
            } else if (type == 0x03) { //Tesselation Shader
            }
        }

        return new ShaderChunk(id, vertShader, fragShader);
    }

    @Override
    public void write(StreamOut s) {
        try {
            s.write(0x10);
            s.write(0x13);
            s.writeInt(this.id.length() * 2);
            s.writeString(id);
            s.writeInt(1 + 1 + 4 + 4 + (vertShader.length() * 2) + (fragShader.length() * 2));

            s.write(0x00);
            s.writeInt(this.vertShader.length() * 2);
            s.writeString(vertShader);

            s.write(0x01);
            s.writeInt(this.fragShader.length() * 2);
            s.writeString(fragShader);
        } catch (IOException ex) {
            Logger.getLogger(ShaderChunk.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getFragShader() {
        return fragShader;
    }

    public void setFragShader(String fragShader) {
        this.fragShader = fragShader;
    }

    public String getVertShader() {
        return vertShader;
    }

    public void setVertShader(String vertShader) {
        this.vertShader = vertShader;
    }
}
