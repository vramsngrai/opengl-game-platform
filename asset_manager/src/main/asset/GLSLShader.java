package main.asset;

//@author David
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GLSLShader {

    public String vertexShader;
    public String fragmentShader;

    private GLSLShader(String vertexShader, String fragmentShader) {
        this.vertexShader = vertexShader;
        this.fragmentShader = fragmentShader;
    }

    public static GLSLShader load(String vertURL, String fragURL) {
        String vert = loadFile(vertURL);
        String frag = loadFile(fragURL);
        return new GLSLShader(vert, frag);
    }

    public static String loadFile(String url) {
        StringBuilder builder = new StringBuilder();
        BufferedReader file = null;
        try {
            file = new BufferedReader(new FileReader(url));
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
            return null;
        }
        try {
            String temp;
            while ((temp = file.readLine()) != null) {
                if (temp.length() > 0) {
                    builder.append(temp).append("\n");
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
            return null;
        }
        return builder.toString();
    }

    public ShaderChunk createChunk(String id) {
        return new ShaderChunk(id, vertexShader, fragmentShader);
    }
}
