package main.asset;

//@author David
public abstract class Chunk {

    public static int TEXTURE_CHUNK = 0;
    public static int SHADER_CHUNK = 1;
    public static int MODEL_CHUNK = 2;
    protected String id;
    protected int type;

    public Chunk(String id, int type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return this.id;
    }

    public int getType() {
        return this.type;
    }

    public abstract void write(StreamOut s);
}
