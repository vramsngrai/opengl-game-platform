package math;

/**
 * @author David Gronlund
 */
public class Quaternion4 {

    float x = 0;
    float y = 0;
    float z = 0;
    float w = 0;

    public Quaternion4() {
    }

    public Quaternion4(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Quaternion4(Vector4 axis, float rotation) {
        setToRotation(axis, rotation);
    }

    public Quaternion4(Quaternion4 value) {
        this.x = value.x;
        this.y = value.y;
        this.z = value.z;
        this.w = value.w;
    }

    public Quaternion4 setToRotation(Vector4 direction, float w) {
        Vector4 temp = new Vector4(direction).normalise();
        this.x = temp.x * GameMath.sin(w / 2);
        this.y = temp.y * GameMath.sin(w / 2);
        this.z = temp.z * GameMath.sin(w / 2);
        this.w = GameMath.cos(w / 2);
        return this;
    }

    public Quaternion4 mult(Quaternion4 operator) {

        this.set(this.x * operator.w + this.w * operator.x + this.y * operator.z - this.z * operator.y,
                this.y * operator.w + this.w * operator.y + this.z * operator.x - this.x * operator.z,
                this.z * operator.w + this.w * operator.z + this.x * operator.y - this.y * operator.x,
                this.w * operator.w - this.x * operator.x - this.y * operator.y - this.z * operator.z);

        return this;
    }

    public Quaternion4 conjugate() {
        this.x *= -1;
        this.y *= -1;
        this.z *= -1;
        return this;
    }

    public Quaternion4 invert() {
        float b = this.x;
        float c = this.y;
        float d = this.z;
        float a = this.w;
        this.conjugate();
        this.x /= b;
        this.y /= c;
        this.z /= d;
        this.w /= a;
        return this;
    }

    public Quaternion4 setIdentity() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.w = 1;
        return this;
    }

    public Quaternion4 normalise() {
        float length = this.length();
        this.w /= length;
        this.x /= length;
        this.y /= length;
        this.z /= length;
        return this;
    }

    public float length() {
        return GameMath.sqrt(GameMath.sqr(this.w) + GameMath.sqr(this.x) + GameMath.sqr(this.y) + GameMath.sqr(this.z));
    }

    public Quaternion4 set(float x, float y, float z, float w) {
        this.w = w;
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    public Matrix3 toMatrix() {
        Matrix3 temp = new Matrix3();

        temp.matrix[0] = 1 - 2 * y * y - 2 * z * z;
        temp.matrix[1] = 2 * x * y + 2 * w * z;
        temp.matrix[2] = 2 * x * z - 2 * w * y;

        temp.matrix[3] = 2 * x * y - 2 * w * z;
        temp.matrix[4] = 1 - 2 * x * x - 2 * z * z;
        temp.matrix[5] = 2 * y * z + 2 * w * x;

        temp.matrix[6] = 2 * x * z + 2 * w * y;
        temp.matrix[7] = 2 * y * z - 2 * w * x;
        temp.matrix[8] = 1 - 2 * x * x - 2 * y * y;

        return temp;
    }

    public Quaternion4 rotate(Vector4 axis, float magnitude) {
        Quaternion4 temp = new Quaternion4(axis, magnitude);
        this.mult(temp);
        return this;
    }

    @Override
    public String toString() {
        return "Quaternion: {" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + "}";
    }
}
