package math;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

/**
 * @author David Gronlund
 */
public class Matrix3 {

    public float[] matrix;

    public Matrix3() {
        matrix = new float[9];
        for (int i = 0; i < 9; i++) {
            matrix[i] = 0;
        }
    }

    public Matrix3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22) {
        matrix = new float[9];
        matrix[0] = m00;
        matrix[1] = m01;
        matrix[2] = m02;
        matrix[3] = m10;
        matrix[4] = m11;
        matrix[5] = m12;
        matrix[6] = m20;
        matrix[7] = m21;
        matrix[8] = m22;
    }

    public Matrix3(Matrix3 data) {
        matrix = new float[9];
        System.arraycopy(data.matrix, 0, matrix, 0, 9);
    }

    public Matrix3(FloatBuffer data) {
        matrix = new float[9];
        for (int i = 0; i < 9; i++) {
            matrix[i] = data.get(i);
        }
    }

    public Matrix3(float[] data) {
        matrix = new float[9];
        System.arraycopy(data, 0, matrix, 0, 9);
    }

    public Matrix3 setIdentity() {
        matrix[0] = 1;
        matrix[1] = 0;
        matrix[2] = 0;
        matrix[3] = 0;
        matrix[4] = 1;
        matrix[5] = 0;
        matrix[6] = 0;
        matrix[7] = 0;
        matrix[8] = 1;
        return this;
    }

    public float get(int column, int row) {
        return GameMath.get(matrix, 3, column, row);
    }

    public Matrix3 set(int column, int row, float value) {
        GameMath.set(matrix, 3, column, row, value);
        return this;
    }

    public FloatBuffer toBuffer() {
        FloatBuffer temp = BufferUtils.createFloatBuffer(9);
        temp.put(matrix);
        return temp;
    }

    public float[] toArray() {
        float temp[] = new float[9];
        System.arraycopy(matrix, 0, temp, 0, 9);
        return temp;
    }

    public Matrix3 fromBuffer(FloatBuffer buf) {
        buf.get(matrix, 0, 9);
        return this;
    }

    public Matrix3 fromArray(float[] array) {
        System.arraycopy(array, 0, matrix, 0, 9);
        return this;
    }

    public Matrix3 transpose() {
        this.matrix = GameMath.transposeMatrix(matrix, 3);
        return this;
    }

    public Matrix3 adjugate() {
        this.matrix = GameMath.adjugateMatrix(matrix, 3);
        return this;
    }

    public Matrix3 add(float operand) {
        this.matrix = GameMath.add(matrix, operand);
        return this;
    }

    public Matrix3 add(Matrix3 operator) {
        this.matrix = GameMath.add(matrix, operator.matrix);
        return this;
    }

    public Matrix3 mult(float operand) {
        this.matrix = GameMath.multiply(matrix, operand);
        return this;
    }

    public Matrix3 mult(Matrix3 operator) {
        this.matrix = GameMath.multiply(matrix, operator.matrix, 3);
        return this;
    }

    public float determinant() {
        return GameMath.determinant(matrix, 3);
    }

    public Matrix3 inverse() {
        this.matrix = GameMath.invert(matrix, 3);
        return this;
    }

    @Override
    public String toString() {
        return "Matrix3: {" + matrix[0] + ", " + matrix[3] + ", " + matrix[6] + ",\n"
                + "          " + matrix[1] + ", " + matrix[4] + ", " + matrix[7] + ",\n"
                + "          " + matrix[2] + ", " + matrix[5] + ", " + matrix[8] + "}";
    }

    public static Vector3 multiplyByVector(Matrix3 mat, Vector3 vec) {
        return new Vector3().fromArray(GameMath.multiplyVector(mat.matrix, vec.toArray(), 3));
    }
}
