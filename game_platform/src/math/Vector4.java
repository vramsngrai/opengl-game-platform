package math;

/**
 * @author David Gronlund
 */
public class Vector4 {

    public float x;
    public float y;
    public float z;
    public float w;

    public Vector4(Vector4 vec) {
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;
        this.w = vec.w;
    }

    public Vector4(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = 1;
    }

    public Vector4(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Vector4(float val) {
        this.x = this.y = this.z = val;
        this.w = 1;
    }

    public Vector4(boolean initToZero) {
        if (initToZero) {
            this.x = this.y = this.z = 0;
            this.w = 1;
        } else {
            this.x = this.y = this.z = 1;
            this.w = 1;
        }
    }

    public Vector4() {
        this.w = 1;
    }

    public float[] toArray() {
        return new float[]{x, y, z, w};
    }

    public float get(int row) {
        if (row == 0) {
            return x;
        } else if (row == 1) {
            return y;
        } else if (row == 2) {
            return z;
        } else if (row == 3) {
            return w;
        }
        return 0;
    }

    public void set(int row, float value) {
        if (row == 0) {
            x = value;
        } else if (row == 1) {
            y = value;
        } else if (row == 2) {
            z = value;
        } else if (row == 3) {
            w = value;
        }
    }

    public Vector4 fromArray(float[] args) {
        this.x = args[0];
        this.y = args[1];
        this.z = args[2];
        this.w = args[3];
        return this;
    }

    public float magnitude() {
        return GameMath.sqrt(GameMath.sqr(x) + GameMath.sqr(y) + GameMath.sqr(z));
    }

    public Vector4 normalise() {
        float mag = this.magnitude();
        this.x /= mag * w;
        this.y /= mag * w;
        this.z /= mag * w;
        this.w = 1;
        return this;
    }

    public Vector4 homogenize() {
        this.x /= w;
        this.y /= w;
        this.z /= w;
        this.w = 1;
        return this;
    }

    public Vector4 translate(Vector4 d) {
        this.x += d.x;
        this.y += d.y;
        this.z += d.z;
        return this;
    }

    public Vector4 translate(float dx, float dy, float dz) {
        this.x += dx;
        this.y += dy;
        this.z += dz;
        return this;
    }

    public Vector4 translate(float d) {
        this.x += d;
        this.y += d;
        this.z += d;
        return this;
    }

    public Vector4 scale(Vector4 s) {
        this.x *= s.x;
        this.y *= s.y;
        this.z *= s.z;
        return this;
    }

    public Vector4 scale(float sx, float sy, float sz) {
        this.x *= sx;
        this.y *= sy;
        this.z *= sz;
        return this;
    }

    public Vector4 scale(float s) {
        this.x *= s;
        this.y *= s;
        this.z *= s;
        return this;
    }

    public Vector4 rotate(Vector4 axis, float rot) {
        Matrix4 temp = new Matrix4().setToRotate(axis, rot);
        this.fromArray(GameMath.multiplyVector(temp.matrix, this.toArray(), 4));
        return this;
    }

    public Vector4 add(Vector4 operand) {
        this.x += operand.x;
        this.y += operand.y;
        this.z += operand.z;
        return this;
    }

    public Vector4 add(float operand) {
        this.x += operand;
        this.y += operand;
        this.z += operand;
        return this;
    }

    public Vector4 subtract(Vector4 operand) {
        this.x -= operand.x;
        this.y -= operand.y;
        this.z -= operand.z;
        return this;
    }

    public Vector4 subtract(float operand) {
        this.x -= operand;
        this.y -= operand;
        this.z -= operand;
        return this;
    }

    public Vector4 multiply(Vector4 operand) {
        this.x *= operand.x;
        this.y *= operand.y;
        this.z *= operand.z;
        return this;
    }

    public Vector4 multiply(float operand) {
        this.x *= operand;
        this.y *= operand;
        this.z *= operand;
        return this;
    }

    public Vector4 divide(Vector4 operand) {
        this.x /= operand.x;
        this.y /= operand.y;
        this.z /= operand.z;
        return this;
    }

    public Vector4 divide(float operand) {
        this.x /= operand;
        this.y /= operand;
        this.z /= operand;
        return this;
    }

    @Override
    public String toString() {
        return "Vector4: {" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + "}";
    }
}
