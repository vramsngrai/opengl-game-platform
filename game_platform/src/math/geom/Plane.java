package math.geom;

import math.*;

/**
 * @author David Gronlund
 */
public class Plane {

    private Vector4 origin;
    private Vector4 normal;

    public Plane(Vector4 origin, Vector4 normal) {
        this.origin = origin;
        this.normal = normal;
        this.normal.normalise();
    }

    public Vector4 getOrigin() {
        return origin;
    }

    public void setOrigin(Vector4 origin) {
        this.origin = origin;
    }

    public Vector4 getNormal() {
        return normal;
    }

    public void setNormal(Vector4 normal) {
        this.normal = normal;
    }

    public static float distance(Plane plane, Vector4 point) {
        return GameMath.dotProduct(new Vector4(point).subtract(plane.getOrigin()), plane.getNormal());
    }

    public static float intersection(Plane plane, Line line) {
        float numerator = GameMath.dotProduct(new Vector4(plane.getOrigin()).subtract(line.getOrigin()),
                plane.getNormal());
        float denominator = GameMath.dotProduct(line.getDirection(), 
                plane.getNormal());
        if (denominator == 0) {
            if (numerator == 0) {
                return Float.POSITIVE_INFINITY;
            } else {
                return Float.NaN;
            }
        }
        return numerator / denominator;
    }
}
