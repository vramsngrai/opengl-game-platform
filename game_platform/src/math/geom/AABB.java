package math.geom;

/**
 * @author David Gronlund
 */
public class AABB {

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float minZ;
    public float maxZ;

    public AABB(float minX, float maxX, float minY, float maxY, float minZ, float maxZ) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.minZ = minZ;
        this.maxZ = maxZ;
    }

    public void fix() {
        if (minX > maxX) {
            float temp = maxX;
            maxX = minX;
            minX = temp;
        }
        if (minY > maxY) {
            float temp = maxY;
            maxY = minY;
            minY = temp;
        }
        if (minZ > maxZ) {
            float temp = maxZ;
            maxZ = minZ;
            minZ = temp;
        }
    }

    public boolean intersects(AABB b) {
        if (b.maxX > minX || maxX > b.minX) {
            return true;
        } else if (b.maxY > minY || maxY > b.minY) {
            return true;
        } else if (b.maxZ > minZ || maxZ > b.minZ) {
            return true;
        } else {
            return false;
        }
    }
}
