package math;

/**
 * @author David Gronlund
 */
public class GameMath {

    public static float PI = (float) Math.PI;

    public static float[] rotate2DValues(float x, float y, float rotation) {
        float newRot = GameMath.toRadians(rotation);
        float newX = (float) Math.cos(newRot) * x - (float) Math.sin(newRot) * y;
        float newY = (float) Math.sin(newRot) * x + (float) Math.cos(newRot) * y;
        float[] temp = new float[2];
        temp[0] = newX;
        temp[1] = newY;
        return temp;
    }

    public static float sin(float arg) {
        return (float) Math.sin(arg);
    }

    public static float cos(float arg) {
        return (float) Math.cos(arg);
    }

    public static float tan(float arg) {
        return (float) Math.tan(arg);
    }

    public static float sqrt(float arg) {
        return (float) Math.sqrt(arg);
    }

    public static float pow(float base, float exponent) {
        return (float) Math.pow(base, exponent);
    }

    public static float sqr(float base) {
        return base * base;
    }

    public static float toDegrees(float radians) {
        radians = (float) (radians % (2 * Math.PI));
        return (float) Math.toDegrees(radians);
    }

    public static float toRadians(float degrees) {
        degrees = degrees % 360;
        return (float) Math.toRadians(degrees);
    }

    public static float normalize(float radians) {
        return (float) (radians % (2 * Math.PI));
    }

    public static Vector3 projectVector(Vector3 axis, Vector3 point) {
        return new Vector3(axis).scale(GameMath.dotProduct(axis, point) / GameMath.sqr(axis.magnitude()));
    }

    public static Vector4 projectVector(Vector4 axis, Vector4 point) {
        axis.homogenize();
        point.homogenize();
        return new Vector4(axis).scale(GameMath.dotProduct(axis, point) / GameMath.sqr(axis.magnitude()));
    }

    public static float dotProduct(Vector3 vec1, Vector3 vec2) {
        return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
    }

    public static float dotProduct(Vector4 vec1, Vector4 vec2) {
        vec1.homogenize();
        vec2.homogenize();
        return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
    }

    public static float dotProduct(float[] arg0, float[] arg1) {
        if (arg0.length == arg1.length) {
            float accum = 0;
            for (int i = 0; i < arg0.length; i++) {
                accum += arg0[i] * arg1[i];
            }
            return accum;
        } else {
            return 0;
        }
    }

    public static boolean vectorsEqual(float[] vec1, float[] vec2) {
        for (int i = 0; i < vec1.length; i++) {
            if (vec1[i] != vec2[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean vectorsEqual(float[] vec1, int offset1, float[] vec2, int offset2, int size) {
        for (int i = 0; i < size; i++) {
            if (vec1[i + offset1] != vec2[i + offset2]) {
                return false;
            }
        }
        return true;
    }

    public static Vector4 vectorAverage(Vector4[] vectors) {
        Vector4 accumulation = new Vector4();
        if (vectors.length < 1) {
            return new Vector4();
        }
        for (int i = 0; i < vectors.length; i++) {
            accumulation.add(vectors[i]);
        }
        accumulation.divide(vectors.length);
        return accumulation;
    }

    public static Vector3 vectorAverage(Vector3[] vectors) {
        Vector3 accumulation = new Vector3();
        if (vectors.length < 1) {
            return new Vector3();
        }
        for (int i = 0; i < vectors.length; i++) {
            accumulation.add(vectors[i]);
        }
        accumulation.divide(vectors.length);
        return accumulation;
    }

    public static float[] vectorAverage(float[][] vectors, int vecLength) {
        float[] accumulation = new float[vecLength];
        if (vectors.length < 1) {
            accumulation[vecLength - 1] = 1;
            return accumulation;
        }
        for (int i = 0; i < vectors.length; i++) {
            float[] vec = vectors[i];
            for (int j = 0; i < vecLength; i++) {
                accumulation[j] += vec[j];
            }
        }
        for (int i = 0; i < vecLength; i++) {
            accumulation[i] /= vectors.length;
        }
        return accumulation;
    }

    public static Vector4 faceNormal(Vector4 p1, Vector4 p2, Vector4 p3) {
        Vector4 planeDef1 = new Vector4(p1).subtract(p2).normalise();
        Vector4 planeDef2 = new Vector4(p2).subtract(p3).normalise();
        return GameMath.crossProduct(planeDef1, planeDef2).normalise();
    }

    public static Vector3 crossProduct(Vector3 vec1, Vector3 vec2) {
        float[] temp = {vec1.x, vec1.y, vec1.z, vec2.x, vec2.y, vec2.z};
        Vector3 result = new Vector3();
        result.x = (temp[1] * temp[5]) - (temp[2] * temp[4]);
        result.y = (temp[0] * temp[5]) - (temp[2] * temp[3]);
        result.z = (temp[0] * temp[4]) - (temp[1] * temp[3]);
        return result;
    }

    public static Vector4 crossProduct(Vector4 vec1, Vector4 vec2) {
        vec1.homogenize();
        vec2.homogenize();
        float[] temp = {vec1.x, vec1.y, vec1.z, vec2.x, vec2.y, vec2.z};
        Vector4 result = new Vector4();
        result.x = (temp[1] * temp[5]) - (temp[2] * temp[4]);
        result.y = (temp[0] * temp[5]) - (temp[2] * temp[3]);
        result.z = (temp[0] * temp[4]) - (temp[1] * temp[3]);
        return result;
    }

    public static Matrix4 findCameraMatrix(Matrix4 coords, boolean rotationOnly) {
        Matrix3 temp = coords.getMatrix3().transpose();
        if (rotationOnly) {
            return new Matrix4().setIdentity().setMatrix3(temp);
        } else {
            Matrix4 translate = new Matrix4().setToTranslate(-coords.matrix[12], -coords.matrix[13], coords.matrix[14]);
            return new Matrix4().setIdentity().setMatrix3(temp).multiply(translate);
        }
    }

    public static Matrix4 frustrumMatrix(float near, float far, float left, float right, float top, float bottom) {
        Matrix4 frustrum = new Matrix4().setIdentity();
        frustrum.matrix[0] = (2.0f * near) / (right - left);
        frustrum.matrix[5] = (2.0f * near) / (top - bottom);
        frustrum.matrix[8] = (right + left) / (right - left);
        frustrum.matrix[9] = (top + bottom) / (top - bottom);
        frustrum.matrix[10] = -(far + near) / (far - near);
        frustrum.matrix[11] = -1.0f;
        frustrum.matrix[14] = -(2.0F * near * far) / (far - near);
        frustrum.matrix[15] = 0.0f;
        return frustrum;
    }

    public static Matrix4 projectionMatrix(float near, float far, float fov, float aspect) {
        float top = near * GameMath.tan((float) ((fov * GameMath.PI) / 360.0));
        float bottom = -top;
        float left = bottom * aspect;
        float right = -left;
        return frustrumMatrix(near, far, left, right, top, bottom);
    }

    public static Matrix4 orthoMatrix(float near, float far, float left, float right, float top, float bottom) {
        Matrix4 frustrum = new Matrix4().setIdentity();
        frustrum.matrix[0] = 2 / (right - left);
        frustrum.matrix[5] = 2 / (top - bottom);
        frustrum.matrix[10] = -2 / (far - near);
        frustrum.matrix[12] = -(right + left) / (right - left);
        frustrum.matrix[13] = -(top + bottom) / (top - bottom);
        frustrum.matrix[14] = -(far + near) / (far - near);
        return frustrum;
    }

    public static float[] add(float[] operand, float[] operator) {
        float[] newMatrix = new float[operand.length];
        for (int i = 0; i < operand.length; i++) {
            newMatrix[i] = operand[i] + operator[i];
        }
        return newMatrix;
    }

    public static float[] multiply(float[] operand, float[] operator, int size) {
        float[] newMatrix = new float[operand.length];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                float accumulation = 0;
                for (int k = 0; k < size; k++) {
                    accumulation += (GameMath.get(operand, size, k, j) * GameMath.get(operator, size, i, k));
                }
                GameMath.set(newMatrix, size, i, j, accumulation);
            }
        }
        return newMatrix;
    }

    public static float[] multiplyVector(float[] operand, float[] operator, int size) {
        float[] newMatrix = new float[size];
        for (int j = 0; j < size; j++) {
            float accumulation = 0;
            for (int k = 0; k < size; k++) {
                accumulation += (GameMath.get(operand, size, k, j) * operator[k]);
            }
            newMatrix[j] = accumulation;
        }
        return newMatrix;
    }

    public static float determinant(float[] matrix, int size) {
        float totals[] = new float[size];
        if (size * size != matrix.length) {
            return 0;
        } else if (size == 1) {
            return matrix[0];
        } else if (size == 2) {
            return (matrix[0] * matrix[3]) - (matrix[2] * matrix[1]);
        }
        if (size > 2) {
            for (int i = 0; i < size; i++) {
                float tempArray[] = new float[(size - 1) * (size - 1)];
                float scalar = get(matrix, size, i, 0);
                for (int j = 0; j < size; j++) {
                    if (j != i) {
                        if (j < i) {
                            for (int k = 0; k < size - 1; k++) {
                                tempArray[(j * (size - 1)) + k] = get(matrix, size, j, k + 1);
                            }
                        } else if (j > i) {
                            for (int k = 0; k < size - 1; k++) {
                                tempArray[((j - 1) * (size - 1)) + k] = get(matrix, size, j, k + 1);
                            }
                        }
                    }
                }
                totals[i] = scalar * determinant(tempArray, size - 1);
            }
        }
        float total = 0;
        boolean positive = true;
        for (int l = 0; l < totals.length; l++) {
            if (positive) {
                total += totals[l];
                positive = false;
            } else {
                total -= totals[l];
                positive = true;
            }
        }
        return total;
    }

    public static float[] cofactorMatrix(float[] matrix, int size, int column, int row) {
        float[] result = new float[(size - 1) * (size - 1)];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i != column) {
                    if (j != row) {
                        int xIndex = 0;
                        int yIndex = 0;
                        if (i < column) {
                            xIndex = i;
                        } else if (i > column) {
                            xIndex = i - 1;
                        }
                        if (j < row) {
                            yIndex = j;
                        } else if (j > row) {
                            yIndex = j - 1;
                        }
                        result[(xIndex * (size - 1)) + yIndex] = matrix[(i * size) + j];
                    }
                }
            }
        }
        return result;
    }

    public static float get(float[] matrix, int size, int column, int row) {
        return matrix[(column * size) + row];
    }

    public static void set(float[] matrix, int size, int column, int row, float value) {
        matrix[(column * size) + row] = value;
    }

    public static boolean isEven(int value) {
        if (value % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static float[] adjugateMatrix(float[] matrix, int size) {
        int counter = 0;
        float[] resultMatrix = new float[matrix.length];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                float[] cofactor = GameMath.cofactorMatrix(matrix, size, i, j);
                float result = 0;
                if (isEven(counter)) {
                    result = GameMath.determinant(cofactor, size - 1);
                } else {
                    result = -GameMath.determinant(cofactor, size - 1);
                }
                GameMath.set(resultMatrix, size, i, j, result);
                counter++;
            }
        }
        resultMatrix = GameMath.transposeMatrix(resultMatrix, size);
        return resultMatrix;
    }

    public static float[] transposeMatrix(float[] matrix, int size) {
        float[] newMatrix = new float[matrix.length];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                GameMath.set(newMatrix, size, j, i, GameMath.get(matrix, size, i, j));
            }
        }
        return newMatrix;
    }

    public static float[] add(float[] matrix, float value) {
        float[] newMatrix = new float[matrix.length];
        System.arraycopy(matrix, 0, newMatrix, 0, matrix.length);
        for (int i = 0; i < newMatrix.length; i++) {
            newMatrix[i] += value;
        }
        return newMatrix;
    }

    public static float[] multiply(float[] matrix, float value) {
        float[] newMatrix = new float[matrix.length];
        System.arraycopy(matrix, 0, newMatrix, 0, matrix.length);
        for (int i = 0; i < newMatrix.length; i++) {
            newMatrix[i] *= value;
        }
        return newMatrix;
    }

    public static float[] invert(float[] matrix, int size) {
        float determinant = GameMath.determinant(matrix, size);
        if (determinant != 0) {
            float[] adjugate = GameMath.adjugateMatrix(matrix, size);
            float inverseDeterminant = 1f / determinant;
            return GameMath.multiply(adjugate, inverseDeterminant);
        }
        return matrix;
    }
}
