package scene;

import java.util.ArrayList;
import math.Matrix4;

/**
 * @author David Gronlund
 */
public class Node {

    public ArrayList<Node> children;
    public Matrix4 transform;
    
    public Node() {
        children = new ArrayList<Node>();
        transform = new Matrix4();
    }
    
    public void render() {
        for (Node n : children) {
            n.render();
        }
    }
}
