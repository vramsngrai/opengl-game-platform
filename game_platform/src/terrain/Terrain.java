package terrain;

import java.util.Random;

/**
 * @author David Gronlund
 */
public class Terrain {

    public static Terrain create(int width, int length, int widthSegments, int lengthSegments) {
        return create(new Random().nextLong(), width, length, widthSegments, lengthSegments);
    }

    public static Terrain create(long seed, int width, int length, int widthSegments, int lengthSegments) {
        Random r = new Random(seed);

        float[] vertices = new float[widthSegments * lengthSegments * 4];

        for (int i = 1; i < widthSegments; i++) {
            for (int j = 0; j < lengthSegments; j++) {
            }
        }

        return new Terrain();
    }
}
