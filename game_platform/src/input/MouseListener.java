package input;

/**
 * @author David Gronlund
 */
public interface MouseListener {
    
    public void onEvent(MouseEvent e);

}
