package input;

/**
 * @author David Gronlund
 */
public interface KeyListener {
    
    public void onEvent(KeyEvent e);

}
