package input;

import org.lwjgl.input.Keyboard;

/**
 * @author David Gronlund
 */
public class KeyEvent {
    
    public final int keyCode;
    public final char keyChar;
    public final boolean keyState;
    public final long nanoseconds;
    
    public KeyEvent() {
        keyCode = Keyboard.getEventKey();
        keyChar = Keyboard.getEventCharacter();
        keyState = Keyboard.getEventKeyState();
        nanoseconds = Keyboard.getEventNanoseconds();
    }
}
