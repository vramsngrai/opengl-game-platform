package main;

import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.gui.DrawableSurface;
import main.mesh.Mesh;
import main.mesh.ObjModelLoader;
import math.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author David Gronlund
 */
public class Main {

    public static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    private RenderableObject triangleBatch;
    private Matrix4 modelMatrix = new Matrix4();
    private Camera camera = new Camera(1, 10000, 45, 1280.0f / 720.0f);
    private Texture texture1;
    private DrawableSurface gui;
    private Shader phong;

    static {
        try {
            LOGGER.addHandler(new FileHandler("errors.log", true));
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, ex.toString(), ex);
        }
    }

    public static void main(String[] args) {
        try {
            Utils.loadNatives();
            System.setProperty("org.lwjgl.librarypath", System.getProperty("java.io.tmpdir"));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        Main main = null;
        try {
            main = new Main();
            main.create();
            main.run();
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
        } finally {
            if (main != null) {
                main.destroy();
            }
        }
    }

    public void create() throws LWJGLException {
        Display.setDisplayMode(new DisplayMode(1280, 720));
        Display.setTitle("Test Platform rev.001 ©2012 101010 Software");
        Display.setVSyncEnabled(true);
        try {
            Display.create(new PixelFormat(), new ContextAttribs(4, 2).withProfileCompatibility(true));
        } catch (Exception ex) {
            Display.create(new PixelFormat(), new ContextAttribs(3, 2).withProfileCompatibility(true));
        }
        Keyboard.create();
        Mouse.setGrabbed(true);
        Mouse.create();
        initGL();
    }

    public void destroy() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }

    public void initGL() {
        glEnable(GL_BLEND);
        glEnable(GL_POLYGON_SMOOTH);
        glEnable(GL_DEPTH_TEST);
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
        glClearColor(0f, 0f, 0f, 1.0f);

        String version = glGetString(GL_VERSION);
        System.out.println(version);

        //AssetFile assets = AssetFile.load(new File("assets/test.ten"));
        //ModelChunk model = (ModelChunk) assets.findChunk("bmw");
        //TextureChunk tex1 = (TextureChunk) assets.findChunk("tex1");
        //Chunk[] chunks = assets.getChunks();
        Mesh mesh = ObjModelLoader.load("assets/models/bunny.obj")[0];
        //ByteBuffer buf = ByteBuffer.allocateDirect(tex1.getData().length);
        //buf.put(tex1.getData());
        //buf.flip();
        //texture1 = new Texture(buf, tex1.getWidth(), tex1.getHeight());
        //texture1.bindTexture();
        gui = new DrawableSurface(100, 100, -.5f, -.5f, 1f, 1f);
        phong = new Shader("assets/shaders/camera_light_shader");
        //phong = new Shader("assets/shaders/phong_shader");
        //triangleBatch = new RenderableObject(GL_TRIANGLES, model.getVertices(), null, model.getNormals(), model.getTextureCoordinates(), model.getIndices());
        triangleBatch = new RenderableObject(GL_TRIANGLES, mesh.vertices, null, mesh.normals, mesh.textureCoordinates, mesh.indices);
        glViewport(0, 0, 1280, 720);
    }

    public void run() {
        while (!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
                Mouse.setGrabbed(true);
            } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
                Mouse.setGrabbed(false);
            }
            if (Display.isVisible()) {
                render();
                Display.update();
            }
            Display.sync(60);
        }
    }
    
    public void render() {
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        //modelMatrix.setIdentity();
        camera.handleInput();
        Matrix4 mat = new Matrix4(camera.getProjectionMatrix()).multiply(camera.getViewMatrix());
        phong.useShader();
        phong.setUniformMatrix("mvMatrix", camera.getViewMatrix());
        phong.setUniformMatrix("pMatrix", camera.getProjectionMatrix());
        phong.setUniformVector("vColor", new Vector4(1.0f, 0, 0, 1.0f));
        triangleBatch.draw(phong.getAttributes());
        //System.out.println("Beanz");
        //value += .1f;
//        Vector4 pos = new Vector4(5, 5, 0);
//        for (int i = 0; i < 5; i++) {
//            for (int j = 0; j < 5; j++) {
//                modelMatrix.setIdentity();
//                modelMatrix.translate(i * 10, 0, j * 10);
//                phong.useShader();
//                phong.setUniformMatrix("mMatrix", modelMatrix);
//                phong.setUniformMatrix("vMatrix", camera.getViewMatrix());
//                phong.setUniformMatrix("pMatrix", camera.getProjectionMatrix());
//                phong.setUniformVector("ambient", new Vector4(.25f, 0f, 0f, 1));
//                phong.setUniformVector("diffuse", new Vector4(.75f, 0f, 0f, 1));
//                phong.setUniformVector("lightPos", pos);
//                phong.setUniformFloat("specular", 8);
//                phong.setUniformInteger("cameraLight", 1);
//                triangleBatch.draw(phong.getAttributes());
//            }
//        }
        //gui.draw();
    }
}
