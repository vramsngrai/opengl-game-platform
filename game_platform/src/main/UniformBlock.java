package main;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL31;
import static org.lwjgl.opengl.GL31.*;

/**
 * @author David Gronlund
 */
public class UniformBlock {
    
    private final int program;
    private final int blockIndex;
    private final int blockSize;
    private Map<String, Integer> uniformLocations;
    private ByteBuffer buffer;
    
    public UniformBlock(Shader shader, String name) {
        this.program = shader.program;
        this.blockIndex = glGetUniformBlockIndex(program, name);
        this.blockSize = glGetActiveUniformBlocki(program, blockIndex, GL_UNIFORM_BLOCK_DATA_SIZE);
        int uniforms = glGetActiveUniformBlocki(program, blockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS);
        for (int i = 0; i < uniforms; i++) {
            //String name = GL31.glGetActiveUniformBlockName(program, blockIndex, blockSize)
        }
        //int maxUniformLength = glGetActiveUniformBlocki(program, blockIndex, GL31.GL_U);
        this.buffer = BufferUtils.createByteBuffer(blockSize);
        this.uniformLocations = new HashMap<String, Integer>();
        
    }
}
