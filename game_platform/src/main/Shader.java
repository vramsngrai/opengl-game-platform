package main;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import math.Matrix4;
import math.Vector3;
import math.Vector4;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL20.*;

/**
 * @author David Gronlund
 */
public class Shader {

    public final int program;
    private Map<String, Integer> attributeLocations = new HashMap<String, Integer>();
    private Map<String, Integer> uniformLocations = new HashMap<String, Integer>();

    public Shader(String vertexSrc, String fragmentSrc) {
        int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, vertexSrc);
        glCompileShader(vertexShader);

        String vertexLog = glGetShaderInfoLog(vertexShader, 65536);
        if (vertexLog.length() != 0) {
            System.out.println("Vertex Shader Error Log:\n" + vertexLog);
        }
        
        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, fragmentSrc);
        glCompileShader(fragmentShader);

        String fragmentLog = glGetShaderInfoLog(vertexShader, 65536);
        if (fragmentLog.length() != 0) {
            System.out.println("Fragment Shader Error Log:\n" + fragmentLog);
        }

        program = glCreateProgram();

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);

        String programLog = glGetProgramInfoLog(program, 65536);
        if (programLog.length() != 0) {
            System.out.println("Program Error Log:\n" + programLog);
        }

        int numAttributes = glGetProgrami(program, GL_ACTIVE_ATTRIBUTES);
        int maxAttributeLength = glGetProgrami(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH);
        for (int i = 0; i < numAttributes; i++) {
            String name = glGetActiveAttrib(program, i, maxAttributeLength);
            int location = glGetAttribLocation(program, name);
            attributeLocations.put(name, location);
        }

        int numUniforms = glGetProgrami(program, GL_ACTIVE_UNIFORMS);
        int maxUniformLength = glGetProgrami(program, GL_ACTIVE_UNIFORM_MAX_LENGTH);
        for (int i = 0; i < numUniforms; i++) {
            String name = glGetActiveUniform(program, i, maxUniformLength);
            int location = glGetUniformLocation(program, name);
            uniformLocations.put(name, location);
        }
    }

    public Shader(String shaderFileURL) {
        this(Utils.loadFile(shaderFileURL + ".vert"), Utils.loadFile(shaderFileURL + ".frag"));
    }

    public void useShader() {
        glUseProgram(program);
    }

    public int getProgram() {
        return program;
    }

    public void setUniformInteger(String id, int num) {
        int location = uniformLocations.get(id);
        glUniform1i(location, num);
    }
    
    public void setUniformFloat(String id, float num) {
        int location = uniformLocations.get(id);
        glUniform1f(location, num);
    }

    public void setUniformVector(String id, Vector4 vec) {
        int location = uniformLocations.get(id);
        glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
    }

    public void setUniformVector(String id, Vector3 vec) {
        int location = uniformLocations.get(id);
        glUniform3f(location, vec.x, vec.y, vec.z);
    }

    public void setUniformMatrix(String id, Matrix4 mat) {
        int location = uniformLocations.get(id);
        FloatBuffer temp = mat.toBuffer();
        temp.flip();
        glUniformMatrix4(location, false, temp);
    }

    public void setUniformMatrix(String id, FloatBuffer mat) {
        int location = uniformLocations.get(id);
        glUniformMatrix4(location, false, mat);
    }
    
    public void setUniformVectorArray(String id, Vector4[] vectors) {
        int location = uniformLocations.get(id);
        float[] values = new float[vectors.length * 4];
        for (int i = 0; i < vectors.length * 4; i += 4) {
            values[i] = vectors[i / 4].x;
            values[i + 1] = vectors[i / 4].y;
            values[i + 2] = vectors[i / 4].z;
            values[i + 3] = vectors[i / 4].w;
        }
        FloatBuffer temp = BufferUtils.createFloatBuffer(values.length);
        temp.put(values);
        temp.flip();
        glUniform4(location, temp);
    }

    public Map getAttributes() {
        return attributeLocations;
    }

    public Map getUniforms() {
        return uniformLocations;
    }
}
