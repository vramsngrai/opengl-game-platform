package main.gui;

import java.nio.ByteBuffer;
import main.RenderableObject;
import main.Shader;
import main.Texture;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author David Gronlund
 */
public class DrawableSurface {

    private RenderableObject surface;
    private Shader shader;
    private ByteBuffer data;
    private Texture texture;

    public DrawableSurface(int pixelsWidth, int pixelsHeight, float x, float y, float width, float height) {
        surface = new RenderableObject(GL_QUADS,
                new float[]{x, y, 0f, 1,
                    x, y + height, 0f, 1,
                    x + width, y + height, 0f, 1,
                    x + width, y, 0f, 1},
                null,
                null,
                new float[]{0, 0, 0, 1,
                    0, 1, 0, 1,
                    1, 1, 0, 1,
                    1, 0, 0, 1},
                new int[]{0, 3, 2, 1});
        shader = new Shader("assets/shaders/gui_shader");
        data = BufferUtils.createByteBuffer(pixelsWidth * pixelsHeight * 4);
        for (int i = 0; i < pixelsWidth * pixelsHeight; i++) {
            data.put((byte)255);
            data.put((byte)255);
            data.put((byte)255);
            data.put((byte)0xff);
        }
        data.flip();
        texture = new Texture(data, pixelsWidth, pixelsHeight);
    }

    public void draw() {
        shader.useShader();
        texture.bindTexture();
        texture.reloadTexture();
        glDisable(GL_DEPTH_TEST);
        surface.draw(shader.getAttributes());
        glEnable(GL_DEPTH_TEST);
    }
}
