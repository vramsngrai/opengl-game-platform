package main;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author David Gronlund
 */
public class Texture {

    private int id;
    private ByteBuffer data;
    private int width;
    private int height;

    public static Texture loadTexture(String url) throws IOException {
        InputStream in = new FileInputStream(url);
        Image tempImage = ImageIO.read(in);
        BufferedImage image = new BufferedImage(tempImage.getWidth(null), tempImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        image.getGraphics().drawImage(tempImage, 0, 0, null);
        int pixels[] = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF)); //Red
                buffer.put((byte) ((pixel >> 8) & 0xFF)); //Green
                buffer.put((byte) (pixel & 0xFF)); //Blue
                buffer.put((byte) ((pixel >> 24) & 0xFF)); //Alpha
            }
        }
        buffer.flip();
        return new Texture(buffer, image.getWidth(), image.getHeight());
    }

    public void bindTexture() {
        glBindTexture(GL_TEXTURE_2D, id);
    }
    
    public void reloadTexture() {
        bindTexture();
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    }

    public Texture(ByteBuffer data, int width, int height) {
        this.data = data;
        this.width = width;
        this.height = height;
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    }
}
