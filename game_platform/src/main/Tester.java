package main;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author David Gronlund
 */
public class Tester implements Runnable {

    public static int[] ints = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public static void main(String[] args) {
        new Thread(new Tester(), "1").start();
        new Thread(new Tester(), "2").start();
        new Thread(new Tester(), "3").start();
        new Thread(new Tester(), "4").start();
        new Thread(new Tester(), "5").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            synchronized (Tester.class) {
                System.out.println(Thread.currentThread().getName() + ", " + ints[i]);
                ints[i] = 10;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
