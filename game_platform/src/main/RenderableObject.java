package main;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Map;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

/**
 * @author David Gronlund
 */
public class RenderableObject {

    private int mode;
    private int elements;
    private int vertexBuffer = -1;
    private int colorBuffer = -1;
    private int textureBuffer = -1;
    private int indexBuffer = -1;
    private int normalBuffer = -1;

    public RenderableObject(int mode, float[] vertices, int[] indices) {
        this.mode = mode;
        elements = indices.length;
        vertexBuffer = createGLArrayBuffer(vertices);
        indexBuffer = createGLElementArrayBuffer(indices);
    }

    public RenderableObject(int mode, float[] vertices, float[] colors, float[] normals, float[] textures, int[] indices) {
        this.mode = mode;
        elements = indices.length;
        vertexBuffer = createGLArrayBuffer(vertices);
        if (colors != null) {
            colorBuffer = createGLArrayBuffer(colors);
        }
        if (normals != null) {
            normalBuffer = createGLArrayBuffer(normals);
        }
        if (textures != null) {
            textureBuffer = createGLArrayBuffer(textures);
        }
        indexBuffer = createGLElementArrayBuffer(indices);
    }

    public final int createGLElementArrayBuffer(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        int location = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, location);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
        return location;
    }

    public final int createGLArrayBuffer(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        int location = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, location);
        glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
        return location;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void draw(Map<String, Integer> attributes) {
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        int vertexLocation = attributes.get("inVertex");
        glVertexAttribPointer(vertexLocation, 4, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(vertexLocation);

        if (attributes.containsKey("inColor")) {
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            int colorLocation = attributes.get("inColor");
            glVertexAttribPointer(colorLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(colorLocation);
        }

        if (attributes.containsKey("inNormal")) {
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            int normalLocation = attributes.get("inNormal");
            glVertexAttribPointer(normalLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(normalLocation);
        }

        if (attributes.containsKey("inTexCoord0")) {
            glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
            int textureLocation = attributes.get("inTexCoord0");
            glVertexAttribPointer(textureLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(textureLocation);
        }

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glDrawElements(mode, elements, GL_UNSIGNED_INT, 0);
    }
}
