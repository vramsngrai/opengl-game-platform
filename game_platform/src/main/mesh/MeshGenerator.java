package main.mesh;

import math.Vector4;

/**
 * @author David Gronlund
 */
public class MeshGenerator {

    public static Mesh generate(Vector4 location, float width, float height, float depth) {
        StringBuilder s = new StringBuilder();
        s.append("v 0.0 0.0 0.0");
        s.append("v " + width + " 0.0 0.0");
        s.append("v " + width + " " + height + " 0.0");
        s.append("v 0.0 " + height + " 0.0");
        s.append("v 0.0 0.0 " + depth);
        s.append("v " + width + " 0.0 " + depth);
        s.append("v " + width + " " + height + " " + depth);
        s.append("v 0.0 " + height + " " + depth);
        
        s.append("vn 0.0, -1.0, 0.0");
        s.append("vn 0.0, 1.0, 0.0");
        s.append("vn 0.0, 0.0, 1.0");
        s.append("vn 1.0, 0.0, 0.0");
        s.append("vn 0.0, 0.0, -1.0");
        s.append("vn -1.0, 0.0, 0.0");
        
        s.append("vt 1.0 0.0 0.0");
        s.append("vt 1.0 1.0 0.0");
        s.append("vt 0.0 1.0 0.0");
        s.append("vt 0.0 0.0 0.0");
        

        return null;
    }

    private static void setVector(float[] array, int offset, float x, float y, float z, float w) {
        array[offset] = x;
        array[offset + 1] = y;
        array[offset + 2] = z;
        array[offset + 3] = w;
    }
    
    public static Mesh createRectangle(Vector4 v1, Vector4 v2, Vector4 v3, Vector4 v4) {
        return new Mesh(new int[]{0, 3, 2, 1},
                new float[]{v1.x, v1.y, v1.z, v1.w,
                    v2.x, v2.y, v2.z, v2.w,
                    v3.x, v3.y, v3.z, v3.w,
                    v4.x, v4.y, v4.z, v4.w},
                null,
                new float[]{0, 0, 0, 1,
                    0, 1, 0, 1,
                    1, 1, 0, 1,
                    1, 0, 0, 1});
    }
}
