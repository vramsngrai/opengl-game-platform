package main.mesh;

import java.util.ArrayList;
import java.util.Scanner;
import main.Utils;

/**
 * @author David Gronlund
 */
public class ObjModelLoader {

    private static ArrayList<IndexBundle> indices = new ArrayList<IndexBundle>();
    private static int currentObject = 0;

    public static class IndexBundle {

        public IntList vertexIndices;
        public IntList textureIndices;
        public IntList normalIndices;

        public IndexBundle(int faces) {
            vertexIndices = new IntList(faces * 3);
            textureIndices = new IntList(faces * 3);
            normalIndices = new IntList(faces * 3);
        }
    }

    public static Mesh[] load(String location) {
        long start = System.currentTimeMillis();
        String file = Utils.loadFile(location);
        Scanner scan = new Scanner(file);
        String[] objectStrings = file.split("\no ");
        if (objectStrings.length > 0) {
            for (int i = 1; i < objectStrings.length; i++) {
                indices.add(new IndexBundle(objectStrings[i].split("\nf ").length - 1));
            }
        } else {
            indices.add(new IndexBundle(file.split("\nf ").length - 1));
        }
        //System.out.println("Objects: " + indices.size());

        FloatList vertices = new FloatList();
        FloatList textures = new FloatList();
        FloatList normals = new FloatList();
        currentObject = -1;

        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            line = line.trim();
            if (line.charAt(0) == '#') {
                //Handle Comment
            } else if (line.charAt(0) == 'v') {
                if (line.charAt(1) != 't' && line.charAt(1) != 'n' && line.charAt(1) != 'p') {
                    parseVertex(vertices, line.substring(1).trim());
                } else if (line.charAt(1) == 't') {
                    parseTexture(textures, line.substring(2).trim());
                } else if (line.charAt(1) == 'n') {
                    parseNormal(normals, line.substring(2).trim());
                }
            } else if (line.charAt(0) == 'f') {
                parseFace(line.substring(1).trim());
            } else if (line.charAt(0) == 'g') {
                //Handle Group Tag
            } else if (line.charAt(0) == 'o') {
                currentObject++;
            } else if (line.charAt(0) == 'u') {
                //Handle Library Usage
            } else if (line.charAt(0) == 'm') {
                //Handle Library Import
            }
        }

        Mesh[] results = new Mesh[indices.size()];
        System.out.println("Time: " + (System.currentTimeMillis() - start));
        if (indices.get(0).textureIndices.get(0) == -1) {
            results[0] = ModelLoaderUtils.combineIndicesWithoutTextures(indices.get(0),
                    vertices.getArray(), normals.getArray());
        } else {
            results[0] = ModelLoaderUtils.combineIndicesWithTextures(indices.get(0),
                    vertices.getArray(), textures.getArray(), normals.getArray());
        }
        return results;
    }

    private static void parseVertex(FloatList vertices, String value) {
        String[] s = value.split(" ");
        vertices.add(Float.parseFloat(s[0]));
        vertices.add(Float.parseFloat(s[1]));
        vertices.add(Float.parseFloat(s[2]));
        vertices.add(1.0f);
    }

    private static void parseTexture(FloatList textures, String value) {
        String[] s = value.split(" ");
        if (s.length == 2) {
            textures.add(Float.parseFloat(s[0]));
            textures.add(Float.parseFloat(s[1]));
            textures.add(0.0f);
            textures.add(1.0f);
        } else {
            textures.add(Float.parseFloat(s[0]));
            textures.add(Float.parseFloat(s[1]));
            textures.add(Float.parseFloat(s[2]));
            textures.add(1.0f);
        }
    }

    private static void parseNormal(FloatList normals, String value) {
        String[] s = value.split(" ");
        normals.add(Float.parseFloat(s[0]));
        normals.add(Float.parseFloat(s[1]));
        normals.add(Float.parseFloat(s[2]));
        normals.add(1.0f);
    }

    private static void parseFace(String value) {
        String[] s = value.split(" ");
        parseFaceGroup(s[0]);
        parseFaceGroup(s[1]);
        parseFaceGroup(s[2]);
    }

    private static void parseFaceGroup(String value) {
        String[] s = value.split("/");
        IndexBundle object = indices.get(currentObject);
        if (s.length == 1) { //Vertex
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(-1);
            object.normalIndices.add(-1);
        } else if (s.length == 2) { //Vertex, Texture
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(Integer.parseInt(s[1]) - 1);
            object.normalIndices.add(-1);
        } else if (s[1].length() == 0) { //Vertex, Normal
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(-1);
            object.normalIndices.add(Integer.parseInt(s[2]) - 1);
        } else { //Vertex, Normal, Texture
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(Integer.parseInt(s[1]) - 1);
            object.normalIndices.add(Integer.parseInt(s[2]) - 1);
        }
    }
}
