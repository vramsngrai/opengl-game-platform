package main.mesh;

import java.util.HashMap;
import java.util.Map;
import main.mesh.ObjModelLoader.IndexBundle;

/**
 * @author David Gronlund
 */
public class ModelLoaderUtils {

    public static Mesh combineIndicesWithTextures(IndexBundle object, float[] vertices, float[] textures, float[] normals) {
        int offset = vertices.length;
        Map<Integer, Integer> map = new HashMap();
        FloatList newVertices = new FloatList(vertices);
        FloatList newTextures = new FloatList(vertices.length, -1);
        FloatList newNormals = new FloatList(vertices.length, -1);
        IntList newIndices = new IntList(object.vertexIndices.getArray());

        for (int i = 0; i < object.vertexIndices.size(); i++) {
            int oldVertexI = object.vertexIndices.get(i);
            int oldTextureI = object.textureIndices.get(i);
            int oldNormalI = object.normalIndices.get(i);
            if (newNormals.get(oldVertexI * 4 + 3) == -1 && newTextures.get(oldVertexI * 4 + 3) == -1) {
                newNormals.copy(oldVertexI * 4, normals, oldNormalI * 4, 4);
                newTextures.copy(oldVertexI * 4, textures, oldTextureI * 4, 4);
            } else if (newNormals.sectionsEqual(oldVertexI * 4, normals, oldNormalI * 4, 4)
                    && newTextures.sectionsEqual(oldVertexI * 4, textures, oldTextureI * 4, 4)) {
            } else {
                Integer newLocation = map.get(oldVertexI);
                if (newLocation != null
                        && newNormals.sectionsEqual(newLocation * 4, normals, oldNormalI * 4, 4)
                        && newTextures.sectionsEqual(newLocation * 4, textures, oldTextureI * 4, 4)) {
                    newIndices.set(i, newLocation);
                } else {
                    newIndices.set(i, offset / 4);
                    newVertices.copy(offset, vertices, oldVertexI * 4, 4);
                    newNormals.copy(offset, normals, oldNormalI * 4, 4);
                    newTextures.copy(offset, textures, oldTextureI * 4, 4);
                    map.put(oldVertexI, offset / 4);
                    offset += 4;
                }
            }
        }
        return new Mesh(newIndices.getArray(), newVertices.getArray(), newTextures.getArray(), newNormals.getArray());
    }

    public static Mesh combineIndicesWithoutTextures(IndexBundle object, float[] vertices, float[] normals) {
        int offset = vertices.length;
        Map<Integer, Integer> map = new HashMap();
        FloatList newVertices = new FloatList(vertices);
        FloatList newNormals = new FloatList(vertices.length, -1);
        IntList newIndices = new IntList(object.vertexIndices.getArray());

        for (int i = 0; i < object.vertexIndices.size(); i++) {
            int oldVertexI = object.vertexIndices.get(i);
            int oldNormalI = object.normalIndices.get(i);
            if (newNormals.get(oldVertexI * 4 + 3) == -1) {
                newNormals.copy(oldVertexI * 4, normals, oldNormalI * 4, 4);
            } else if (newNormals.sectionsEqual(oldVertexI * 4, normals, oldNormalI * 4, 4)) {
            } else {
                Integer newLocation = map.get(oldVertexI);
                if (newLocation != null
                        && newNormals.sectionsEqual(newLocation * 4, normals, oldNormalI * 4, 4)) {
                    newIndices.set(i, newLocation);
                } else {
                    newIndices.set(i, offset / 4);
                    newVertices.copy(offset, vertices, oldVertexI * 4, 4);
                    newNormals.copy(offset, normals, oldNormalI * 4, 4);
                    map.put(oldVertexI, offset / 4);
                    offset += 4;
                }
            }
        }
        return new Mesh(newIndices.getArray(), newVertices.getArray(), null, newNormals.getArray());
    }
}
