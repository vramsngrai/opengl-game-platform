#version 420

in vec3 normal;
in vec3 lightDirection;
in vec4 vertex;

out vec4 pixel;

uniform vec4 ambient;
uniform vec4 diffuse;
uniform float specular;

void main() {
    float diff = max(0.0, dot(normalize(normal), lightDirection));
    pixel = diff * diffuse;
    pixel += ambient;

    vec3 reflection = normalize(reflect(-lightDirection, normal));
    //float spec = max(0.0, dot(normalize(normal), reflection));
    float spec = max(0.0, dot(-normalize(vertex).xyz, reflection));
    float fSpec = pow(spec, 128.0) * specular;

    if (diff != 0.0) {
        pixel += vec4(fSpec, fSpec, fSpec, 0);
    }
}