#version 420

in vec3 normal;
in vec3 lightDirection;
in vec4 vertex;
in float lightDistance;

out vec4 pixel;

uniform vec4 ambient;
uniform vec4 diffuse;
uniform float specular;

void main() {
    float distance = 0;
    if (lightDistance == 0) {
        distance = 4;
    } else {
        distance = (1 / pow(distance, 2));
    }
    float diff = max(0.0, dot(normalize(normal), lightDirection));
    pixel = diff * diffuse * distance;
    pixel += ambient;

    vec3 reflection = normalize(reflect(-lightDirection, normal));
    float spec = max(0.0, dot(normalize(normal), reflection));
    float fSpec = pow(spec, 128.0) * specular;

    if (diff != 0.0) {
        pixel += vec4(fSpec, fSpec, fSpec, 0) * distance;
    }
}