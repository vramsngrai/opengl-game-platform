uniform mat4 mvpMatrix;
attribute vec4 inVertex;
attribute vec4 inTexCoord0;
varying vec4 vTex;
void main(void){ 
	vTex = inTexCoord0;
	gl_Position = mvpMatrix * inVertex; 
}